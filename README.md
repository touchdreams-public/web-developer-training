# Web Developer Training

## Useful resources

- Cheatsheets
- [Can I Use](https://caniuse.com)
- [MDN](https://developer.mozilla.org/en-US/docs/Web)
- [Website: A List Apart](https://alistapart.com)
- [101 Web Development Things You Need to Know | YouTube](https://www.youtube.com/watch?v=erEgovG9WBs)
- [Touchdreams Websites Google Site](https://sites.google.com/view/touchdreams/development?authuser=0)
- [npm trends](https://www.npmtrends.com/)

## General web dev principles

<details>
<summary>Curriculum</summary>

- Frontend & backend
- URLs
- Tech stacks
- Web design (incl. responsive wd)
- Web apps

</details>

<details>
<summary>Helpful links</summary>

- [How to OVER Engineer a Website // What is a Tech Stack? | YouTube](https://www.youtube.com/watch?v=Sxxw3qtb3_g)
- [What is a web app? | Touchdreams](https://websites.touchdreams.co.za/questions-and-answers/what-web-app)
- [Technology Ecosystems Cheat Sheet (pdf)](https://cdn2.hubspot.net/hubfs/450622/Technology_Ecosystems_Cheat_Sheet-1.pdf)

</details>

## Webflow

*A simple way to build sites with just enough control*

<details>
<summary>Curriculum</summary>

- Type of site builder; use cases
- Basic dev
  - HTML / elements available
  - Applying CSS
  - using / organising classes
  - Applying animations
  - Responsive modes
  - Embedding custom components
- CMS
  - Types + fields
  - CMS items in "lists"
  - CMS page templates
- Launching + Admin
  - Using editor mode; incl. training clients
  - Forms
  - Adding a custom domain
  - Handover + other settings

</details>

## CSS

*For making websites beautiful*

<details>
<summary>Things you should already know</summary>

- Colours
- Fonts & type (incl. web fonts)
- Media queries
- Box model

</details>

<details>
<summary>Curriculum</summary>

We assume that the developers already know the basics of CSS, so we'll only be covering advanced and new features of CSS.

- "custom properties" (variables)
- Flexbox (on [CSS Tricks](https://css-tricks.com/snippets/css/a-guide-to-flexbox/))
- Grid (on [CSS Tricks](https://css-tricks.com/snippets/css/complete-guide-grid/))
- Attribute selectors (on [CSS Tricks](https://css-tricks.com/attribute-selectors/))
- Pseudo selectors
- Aspect ratios
  - Incl. `background-size`, `aspect-ratio`, `object-fit`
- Calc
- Transitions & transforms
- Processors
  - Sass ([official site](https://sass-lang.com/)), Less, PostCSS

</details>

<details>
<summary>Helpful links</summary>

- [Learn CSS course on web.dev](https://web.dev/learn/css/)
- [Website: CSS Tricks](https://css-tricks.com)
- [CSS in 100 Seconds | YouTube](https://www.youtube.com/watch?v=OEV8gMkCHXQ)
- [CSS Pseudo-classes in 100 Seconds | YouTube](https://www.youtube.com/watch?v=kpXKwDGtjGE)

</details>

## JavaScript

*For making websites interactive*

<details>
<summary>Curriculum</summary>

- Querying the DOM
- Events
- Promises (incl. `async` / `await`)
- Fetch API
- Modules (ES modules & Node.js modules)

</details>

<details>
<summary>Helpful links</summary>

- [JavaScript Modules in 100 Seconds | YouTube](https://www.youtube.com/watch?v=qgRUr-YUk1Q)
- [Promises in 100 Seconds | YouTube](https://www.youtube.com/watch?v=RvYYCGs45L4)
- [What is THIS in JavaScript? in 100 seconds | YouTube](https://www.youtube.com/watch?v=YOlr79NaAtQ)
- [ES modules: A cartoon deep-dive | Mozilla Hacks](https://hacks.mozilla.org/2018/03/es-modules-a-cartoon-deep-dive/)

</details>

## Development environment

*Getting your computer ready to work on code*

<details>
<summary>Curriculum</summary>
*Note that these are highly OS-specific*

- Code editors / IDEs
- CLI / Terminal
- Browsers
- CLI tools (these are covered in more detail later)
  - built-in (eg. nano, less, wget, etc.)
  - node
  - git
  - docker
  - composer (for PHP)
- Repositories

</details>

<details>
<summary>Helpful links</summary>

- [Git Explained in 100 Seconds | YouTube](https://www.youtube.com/watch?v=hwP7WQkmECE)
- [Bash in 100 Seconds | YouTube](https://www.youtube.com/watch?v=I4EWvMFj37g)
- [Docker in 100 Seconds | YouTube](https://www.youtube.com/watch?v=Gjnup-PuquQ)
- [VS Code in 100 Seconds | YouTube](https://www.youtube.com/watch?v=KMxo3T_MTvY)
- [Excellent Git Cheat Sheet | Gitlab (pdf)](https://about.gitlab.com/images/press/git-cheat-sheet.pdf)

</details>

## Browser dev tools

*For investigating how your browser sees your site / app*

<details>
<summary>Curriculum</summary>

- Inspector
- Console
- Network
- Responsive tester
- Storage

</details>

<details>
<summary>Helpful links</summary>

- [Chrome official devtools docs](https://developer.chrome.com/docs/devtools/overview/); The intro video on that page is alright&hellip; it's also old (mid 2019)
- [Firefox official devtools docs](https://firefox-source-docs.mozilla.org/devtools-user/index.html)
- [21+ Browser Dev Tools & Tips You Need To Know | YouTube](https://www.youtube.com/watch?v=TcTSqhpm80Y)

</details>

## Shopify

*A great way to build regular ecommerce sites*

<details>
<summary>Curriculum</summary>

- Using the backend
  - Products
  - Categories + collections
  - Choosing themes
  - Plugins ("apps")
  - Marketing tools
    - Discounts
  - Info pages
  - Legal pages + other settings
  - Bulk import products
- Custom theme development
  - Developing a theme using Shopify CLI
  - Liquid
  - Template parts?

</details>

## Drupal

*A powerful content management system (CMS)*

<details>
<summary>Curriculum</summary>
<p><em>Including custom themes, custom modules</em></p>

- Structure / setup (5 days)
  - Installation
  - Drupal versions : drupal core
  - Blocks
  - Nodes / Content types
  - Taxonomies (vocabularies)
  - Menus
  - Views
  - Image styles
  - Users
  - Site status page
- Extending (contrib modules) (2 days)
  - Webforms
  - Pathologic
  - Pathauto
  - Search API
  - Devel (+ kint)
  - Devel generate
  - AMSwap
- Theme development (3 days)
  - Not including knowing CSS, HTML, JavaScript
  - Folder structure
  - .info.yml file
  - Sub- themes
  - Templates + Regions
  - Libraries
  - CSS and JS (acknowledge)
  - #advanced preprocess functions
- Custom module development
  - Hooks
  - Forms &amp; render API (see [docs](https://api.drupal.org/api/drupal/elements/8.9.x))
  - Caching

</details>

<details>
<summary>Useful modules</summary>

- [Admin Toolbar](https://www.drupal.org/project/admin_toolbar)
- [AMSwap](https://www.drupal.org/project/amswap) (by Me! (Dane))
- [Backup and Migrate](https://www.drupal.org/project/backup_migrate)
- [Block Class](https://www.drupal.org/project/block_class)
- [Field Group](https://www.drupal.org/project/field_group)
- [Google Tag Manager](https://www.drupal.org/project/google_tag)
  - [Google Analytics](https://www.drupal.org/project/google_analytics)
- [Honeypot](https://www.drupal.org/project/honeypot)
- [Metatag](https://www.drupal.org/project/metatag)
- [Paragraphs](https://www.drupal.org/project/paragraphs)
  - [Geysir](https://www.drupal.org/project/geysir)
- [Pathauto](https://www.drupal.org/project/pathauto)
- [Redirect](https://www.drupal.org/project/redirect)
- [Smart Trim](https://www.drupal.org/project/smart_trim)
- [Stage File Proxy](https://www.drupal.org/project/stage_file_proxy)
- [Webform](https://www.drupal.org/project/webform)
- [Devel](https://www.drupal.org/project/devel)

</details>

<details>
<summary>Helpful links</summary>

- [Drupal notes on Touchdreams Google site](https://sites.google.com/view/touchdreams/development/drupal?authuser=0)
- [Understanding hooks | drupal.org](https://www.drupal.org/docs/creating-custom-modules/understanding-hooks)
- [Dreamy base theme | Gitlab](https://gitlab.com/touchdreams/dreamy)
- [Dreamy theme zip file](http://websites.touchdreams.co.za/old-site/dreamy-master.zip)

</details>

## WordPress

*A popular content management system (CMS) with thousands of plugins and themes*

<details>
<summary>Curriculum</summary>
<p><em>Including custom themes, custom plugins.</em></p>

- Structure / setup (2 days)
  - Installation
  - Choosing themes
  - Customising / editing themes
  - Blocks / Gutenberg
  - Widgets
  - Menus
  - Users + roles
  - Taxonomies: Categories + Tags
- Extending (contrib modules) (2 days)
  - CPT + ACF
  - Facets
  - Forms (WP Forms or Ninja Forms)
- Themes (3 days)
  - Folder structure
  - styles.css
  - "hooks"
- WooCommerce
  - Products

</details>

<details>
<summary>Helpful links</summary>

- [WordPress.org](https://wordpress.org/)
- [WordPress.com](https://wordpress.com/)
- [Block Themes info | WordPress.org](https://developer.wordpress.org/themes/block-themes/)
- [Underscores base theme](https://underscores.me/)
- [Template Hierarchy | wordpress.org](https://developer.wordpress.org/themes/basics/template-hierarchy/)
- [A Deep Introduction to WordPress Block Themes | CSS Tricks](https://css-tricks.com/a-deep-introduction-to-wordpress-block-themes/)

</details>

<details>
<summary>WooCommerce links</summary>

- [Storefront child theme](https://woocommerce.com/storefront/)
- [WC_Product_Simple class docs](https://woocommerce.github.io/code-reference/classes/WC-Product-Simple.html)

</details>

## APIs &amp; integrations

*How your site / app can communicate with other sites and services*

<details>
<summary>Curriculum</summary>

- REST
- GraphQL
- HTTP requests
  - [RESTful APIs in 100 seconds | YouTube](https://www.youtube.com/watch?v=-MTSQjw5DrM)
  - GraphQL
  - Methods
  - Headers
  - Body + JSON
  - JSON:API
- APIs
  - Explore GitHub API
  - Insomnia / Postman
  - Authentication
  - Documentation
  - SwaggerHub
  - API managers (eg. Apigee)

</details>

<details>
<summary>Helpful links</summary>

- [RESTful APIs in 100 Seconds | YouTube](https://www.youtube.com/watch?v=-MTSQjw5DrM)
- [GraphQL in 100 Seconds | YouTube](https://www.youtube.com/watch?v=eIQh02xuVw4)
- [HTTP Status codes cheatsheet | Devhints.io](https://devhints.io/http-status)

</details>

## Frontend (JavaScript) frameworks

*For building interactive web user interfaces*

<details>
<summary>Curriculum</summary>

- Vue
- React
- Angular
- Others
  - Express, Svelte, Solid, Astro, Mithril
- Data binding
- Components
- Routing
- Forms
- SPAs vs SSG vs hydration

</details>

<details>
<summary>Helpful links</summary>

- [Vue.js in 100 Seconds | YouTube](https://www.youtube.com/watch?v=nhBVL41-_Cw)
- [React in 100 Seconds | YouTube](https://www.youtube.com/watch?v=Tn6-PIqc4UM)
- [Angular in 100 Seconds | YouTube](https://www.youtube.com/watch?v=Ata9cSC2WpM)
- [Svelte in 100 Seconds | YouTube](https://www.youtube.com/watch?v=rv3Yq-B8qp4)

</details>

## TypeScript

*Makes JavaScript more reliable*

<details>
<summary>Curriculum</summary>

- [TypeScript in 100 Seconds | YouTube](https://www.youtube.com/watch?v=zQnBQ4tB3ZA)
- Basic syntax
- Classes &amp; interfaces
- CLI tools
- IDE tools

</details>

## Docker &amp; containers

*The modern way to serve sites and apps*

<details>
<summary>Curriculum</summary>

- Getting images
- Building images
- Docker compose
- Traefik
- Dockerhub
- Docker swarm / Kubernetes

</details>

## Git

*To backup and collaborate on code*

<details>
<summary>Curriculum</summary>

- What it is
- Basic commands
- Advanced commands
- Github / Gitlab

</details>

<details>
<summary>Helpful links</summary>

- [Learn Git Branching](https://learngitbranching.js.org/)
- [GitHub cheat sheet (pdf)](https://education.github.com/git-cheat-sheet-education.pdf)
- [Atlassian cheat sheet (pdf)](https://wac-cdn.atlassian.com/dam/jcr:e7e22f25-bba2-4ef1-a197-53f46b6df4a5/SWTM-2088_Atlassian-Git-Cheatsheet.pdf)

</details>

## PWAs

*Progressive web apps: Websites that act like native apps*

<details>
<summary>Curriculum</summary>

- Offline modes
- Caching
- Browser storage
- Service workers
- Installation on phones
- Notifications
- Access to device hardware (eg. bluetooth)

</details>

<details>
<summary>Helpful links</summary>

- [Learn PWA on web.dev](https://web.dev/learn/pwa/)
- [PWAs docs on MDN](https://developer.mozilla.org/en-US/docs/Web/Progressive_web_apps)

</details>

## Cloud services

*Hosting, compute, databases, authentication, etc.*

<details>
<summary>Curriculum</summary>

- AWS by Amazon
- Azure by Microsoft
- Google Cloud (GCP) by Google
- Firebase by Google

</details>

<details>
<summary>Helpful links</summary>

- [Top 50+ AWS Services Explained in 10 Minutes | YouTube](https://www.youtube.com/watch?v=JIbIYCM48to)
- [Firebase in 100 Seconds | YouTube](https://www.youtube.com/watch?v=vAoB4VbhRzM)

</details>

## Other development concepts

### Curriculum

- Markdown
- SEO
- Domains / DNS
- CDNs
- Integration testing
- Unit testing
- Accessibility
- Cross-browser testing
- Regex (see [RegExr](https://regexr.com/))
- WebSockets
- CORS

<details>
<summary>Helpful links</summary>

- [SEO for Developers in 100 Seconds | YouTube](https://www.youtube.com/watch?v=-B58GgsehKQ)
- [DNS in 100 Seconds | YouTube](https://www.youtube.com/watch?v=UVR9lhUGAyU)
- [Software Testing in 100 Seconds | YouTube](https://www.youtube.com/watch?v=u6QfIXgjwGQ)
- [WebSockets in 100 Seconds | YouTube](https://www.youtube.com/watch?v=1BfCnjr_Vjg)
- [CORS in 100 Seconds | YouTube](https://www.youtube.com/watch?v=4KHiSt0oLJ0)

</details>
